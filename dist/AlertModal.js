'use strict';

var React = require('react');
var ReactModal = require('react-modal');

var AlertModal = React.createClass({
  render: function () {
    var btnOk;
    var btnClose;

    if (this.props.btnAction) {
      btnOk = React.createElement(
        'div',
        { className: 'modal-footer' },
        React.createElement(
          'button',
          { type: 'button', className: 'btn btn-default', onClick: this.props.btnAction },
          'OK'
        )
      );

      btnClose = React.createElement(
        'button',
        { type: 'button', className: 'close', onClick: this.props.btnAction },
        React.createElement(
          'span',
          { 'aria-hidden': 'true' },
          '×'
        ),
        React.createElement(
          'span',
          { className: 'sr-only' },
          'Fechar'
        )
      );
    }

    return React.createElement(
      ReactModal,
      {
        className: 'Modal__Bootstrap col-xs-12 col-sm-10 col-md-8 col-lg-6',
        closeTimeoutMS: 250,
        isOpen: this.props.show,
        onRequestClose: this.props.btnAction
      },
      React.createElement(
        'div',
        { className: 'modal-content' },
        React.createElement(
          'div',
          { className: 'modal-header' },
          btnClose,
          React.createElement(
            'h4',
            { className: 'modal-title' },
            String(this.props.title).translate()
          )
        ),
        React.createElement(
          'div',
          { className: 'modal-body' },
          React.createElement(
            'div',
            { className: 'alert alert-'.concat(this.props.type) },
            String(this.props.message).translate()
          )
        ),
        btnOk
      )
    );
  }
});

module.exports = AlertModal;