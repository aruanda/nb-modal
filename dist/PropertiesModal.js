/*globals $:false*/
'use strict';

var React = require('react');
var ReactModal = require('react-modal');

var PropertiesModal = React.createClass({
  handleOnSubmit: function () {
    var img = $(this.props.img);

    img.attr('src', this.refs.imgLink.value);
    img.attr('alt', this.refs.imgDesc.value);
    img.css('width', this.refs.imgWidth.value);
    img.css('height', this.refs.imgHeight.value);

    this.props.onSave(img[0]);
  },

  render: function () {
    var props = this.props;
    var model = {};

    var img = $(this.props.img);

    model = {
      src: img.attr('src'),
      alt: img.attr('alt'),
      width: img.css('width'),
      height: img.css('height')
    };

    return React.createElement(
      ReactModal,
      {
        className: 'Modal__Bootstrap col-xs-12 col-sm-10 col-md-8 col-lg-6',
        closeTimeoutMS: 250,
        isOpen: props.show,
        onRequestClose: props.onCancel
      },
      React.createElement(
        'div',
        { className: 'modal-content' },
        React.createElement(
          'div',
          { className: 'modal-header' },
          React.createElement(
            'button',
            { type: 'button', className: 'close', onClick: props.onCancel, 'aria-label': 'Close' },
            React.createElement(
              'span',
              { 'aria-hidden': 'true' },
              '×'
            )
          ),
          React.createElement(
            'h4',
            { className: 'modal-title' },
            'images.properties.title'.translate()
          )
        ),
        React.createElement(
          'div',
          { className: 'modal-body' },
          React.createElement(
            'div',
            { className: 'row' },
            React.createElement(
              'div',
              { className: 'form-group col-md-12' },
              React.createElement(
                'label',
                { htmlFor: 'imgLink' },
                'Link da imagem'
              ),
              React.createElement(
                'div',
                { className: 'input-group' },
                React.createElement(
                  'label',
                  { htmlFor: 'imgLink', className: 'input-group-addon' },
                  React.createElement('i', { className: 'fa fa-link' })
                ),
                React.createElement('input', {
                  id: 'imgLink',
                  ref: 'imgLink',
                  type: 'text',
                  className: 'form-control',
                  defaultValue: model.src
                })
              )
            ),
            React.createElement(
              'div',
              { className: 'form-group col-md-12' },
              React.createElement(
                'label',
                { htmlFor: 'imgDesc' },
                'Descrição'
              ),
              React.createElement(
                'div',
                { className: 'input-group' },
                React.createElement(
                  'label',
                  { htmlFor: 'imgDesc', className: 'input-group-addon' },
                  React.createElement('i', { className: 'fa fa-file-text-o' })
                ),
                React.createElement('input', {
                  id: 'imgDesc',
                  ref: 'imgDesc',
                  type: 'text',
                  className: 'form-control',
                  defaultValue: model.alt
                })
              )
            ),
            React.createElement(
              'div',
              { className: 'form-group col-md-6' },
              React.createElement(
                'label',
                { htmlFor: 'imgWidth' },
                'Largura'
              ),
              React.createElement(
                'div',
                { className: 'input-group' },
                React.createElement(
                  'label',
                  { htmlFor: 'imgWidth', className: 'input-group-addon' },
                  React.createElement('i', { className: 'fa fa-arrows-h' })
                ),
                React.createElement('input', {
                  id: 'imgWidth',
                  ref: 'imgWidth',
                  type: 'text',
                  className: 'form-control',
                  defaultValue: model.width
                })
              )
            ),
            React.createElement(
              'div',
              { className: 'form-group col-md-6' },
              React.createElement(
                'label',
                { htmlFor: 'imgHeight' },
                'Altura'
              ),
              React.createElement(
                'div',
                { className: 'input-group' },
                React.createElement(
                  'label',
                  { htmlFor: 'imgHeight', className: 'input-group-addon' },
                  React.createElement('i', { className: 'fa fa-arrows-v' })
                ),
                React.createElement('input', {
                  id: 'imgHeight',
                  ref: 'imgHeight',
                  type: 'text',
                  className: 'form-control',
                  defaultValue: model.height
                })
              )
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'modal-footer' },
          React.createElement(
            'div',
            { className: 'col-md-12' },
            React.createElement(
              'a',
              { href: 'javascript:void(0);', onClick: props.onCancel, className: 'marginTop10px text-muted pull-left' },
              'properties.btn.cancel'.translate()
            ),
            React.createElement(
              'button',
              { type: 'button', onClick: this.handleOnSubmit, className: 'btn btn-primary' },
              'properties.btn.save'.translate()
            )
          )
        )
      )
    );
  }
});

module.exports = PropertiesModal;