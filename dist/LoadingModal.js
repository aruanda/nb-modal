'use strict';

var React = require('react');
var ReactModal = require('react-modal');

var LoadingModal = React.createClass({
  render: function () {
    return React.createElement(
      ReactModal,
      {
        className: 'Modal__Bootstrap col-xs-12 col-sm-10 col-md-8 col-lg-6',
        closeTimeoutMS: 250,
        isOpen: this.props.show
      },
      React.createElement(
        'div',
        { className: 'modal-content' },
        React.createElement(
          'div',
          { className: 'modal-header' },
          React.createElement(
            'h4',
            { className: 'modal-title' },
            this.props.title.translate()
          )
        ),
        React.createElement(
          'div',
          { className: 'modal-body' },
          React.createElement(
            'div',
            { className: 'row' },
            React.createElement(
              'div',
              { className: 'col-md-12' },
              React.createElement(
                'div',
                { className: 'alert alert-success alert-dismissable' },
                React.createElement(
                  'h5',
                  null,
                  this.props.message.translate()
                )
              ),
              React.createElement(
                'div',
                { className: 'progress active', style: { height: '40px' } },
                React.createElement(
                  'div',
                  { className: 'progress-bar progress-bar-warning progress-bar-striped', role: 'progressbar', 'aria-valuenow': '40', 'aria-valuemin': '0', 'aria-valuemax': '100', style: { width: '100%', height: '100%' } },
                  React.createElement(
                    'span',
                    { className: 'sr-only' },
                    ' '
                  )
                )
              )
            )
          )
        )
      )
    );
  }
});

module.exports = LoadingModal;