'use strict';

var React = require('react');
var ReactModal = require('react-modal');

var FormModal = React.createClass({
  render: function () {
    return React.createElement(
      ReactModal,
      {
        className: 'Modal__Bootstrap col-xs-12 col-sm-10 col-md-8 col-lg-6',
        closeTimeoutMS: 250,
        isOpen: this.props.show,
        onRequestClose: this.props.onLeave
      },
      React.createElement(
        'div',
        { className: 'modal-content' },
        React.createElement(
          'div',
          { className: 'modal-header' },
          React.createElement(
            'button',
            { type: 'button', className: 'close', onClick: this.props.onLeave, 'aria-label': 'Close' },
            React.createElement(
              'span',
              { 'aria-hidden': 'true' },
              '×'
            )
          ),
          React.createElement(
            'h4',
            { className: 'modal-title' },
            String(this.props.title).translate()
          )
        ),
        React.createElement(
          'div',
          { className: 'modal-body' },
          React.createElement(
            'div',
            { className: 'col-md-3 col-sm-6 col-xs-12' },
            React.createElement(
              'div',
              { className: 'info-box bg-red' },
              React.createElement(
                'span',
                { className: 'info-box-icon' },
                React.createElement('i', { className: 'fa fa-comments-o' })
              ),
              React.createElement(
                'div',
                { className: 'info-box-content' },
                React.createElement(
                  'span',
                  { className: 'info-box-text' },
                  'Comments'
                ),
                React.createElement(
                  'span',
                  { className: 'info-box-number' },
                  '41,410'
                ),
                React.createElement(
                  'div',
                  { className: 'progress' },
                  React.createElement('div', { className: 'progress-bar width70porcent' })
                ),
                React.createElement(
                  'span',
                  { className: 'progress-description' },
                  '70% Increase in 30 Days'
                )
              )
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'modal-footer' },
          React.createElement(
            'div',
            { className: 'col-md-12' },
            React.createElement(
              'a',
              { href: 'javascript:void(0);', className: 'marginTop10px text-muted pull-left', onClick: this.props.onLeave },
              'Fechar'
            ),
            React.createElement(
              'a',
              { href: 'javascript:void(0);', className: 'marginTop10px text-muted pull-left', onClick: this.props.onBack },
              'Listagem'
            ),
            React.createElement(
              'button',
              { type: 'button', className: 'btn btn-primary pull-right', onClick: this.props.onView },
              'Ver'
            )
          )
        )
      )
    );
  }
});

module.exports = FormModal;