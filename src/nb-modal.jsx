'use strict';

var FormModal = require('./FormModal');
var AlertModal = require('./AlertModal');
var LoadingModal = require('./LoadingModal');
var PropertiesModal = require('./PropertiesModal');

module.exports = {
  FormModal: FormModal,
  AlertModal: AlertModal,
  LoadingModal: LoadingModal,
  PropertiesModal: PropertiesModal
};
