'use strict';

var React = require('react');
var ReactModal = require('react-modal');

var LoadingModal = React.createClass({
  render: function() {
    return (
      <ReactModal
        className={'Modal__Bootstrap col-xs-12 col-sm-10 col-md-8 col-lg-6'}
        closeTimeoutMS={250}
        isOpen={this.props.show}
      >
        <div className="modal-content">
          <div className="modal-header">
            <h4 className="modal-title">
              { this.props.title.translate() }
            </h4>
          </div>

          <div className="modal-body">
            <div className="row">
              <div className="col-md-12">
                <div className="alert alert-success alert-dismissable">
                  <h5>{ this.props.message.translate() }</h5>
                </div>

                <div className="progress active" style={{ height: '40px' }}>
                  <div className="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={{ width: '100%', height: '100%' }}>
                    <span className="sr-only">&nbsp;</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </ReactModal>
    );
  }
});

module.exports = LoadingModal;
