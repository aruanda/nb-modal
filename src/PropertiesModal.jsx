/*globals $:false*/
'use strict';

var React      = require('react');
var ReactModal = require('react-modal');

var PropertiesModal = React.createClass({
  handleOnSubmit: function() {
    var img = $(this.props.img);

    img.attr('src',   this.refs.imgLink.value);
    img.attr('alt',   this.refs.imgDesc.value);
    img.css('width',  this.refs.imgWidth.value);
    img.css('height', this.refs.imgHeight.value);

    this.props.onSave(img[0]);
  },

  render: function() {
    var props = this.props;
    var model = {};

    var img = $(this.props.img);

    model = {
      src:    img.attr('src'),
      alt:    img.attr('alt'),
      width:  img.css('width'),
      height: img.css('height')
    };

    return (
      <ReactModal
        className={'Modal__Bootstrap col-xs-12 col-sm-10 col-md-8 col-lg-6'}
        closeTimeoutMS={250}
        isOpen={props.show}
        onRequestClose={props.onCancel}
      >
        <div className="modal-content">
          <div className="modal-header">
            <button type="button" className="close" onClick={props.onCancel} aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 className="modal-title">
              { 'images.properties.title'.translate() }
            </h4>
          </div>

          <div className="modal-body">
            <div className="row">
              <div className="form-group col-md-12">
                <label htmlFor="imgLink">Link da imagem</label>
                <div className="input-group">
                  <label htmlFor="imgLink" className="input-group-addon">
                    <i className="fa fa-link"></i>
                  </label>

                  <input
                    id="imgLink"
                    ref="imgLink"
                    type="text"
                    className="form-control"
                    defaultValue={model.src}
                  />
                </div>
              </div>

              <div className="form-group col-md-12">
                <label htmlFor="imgDesc">Descrição</label>
                <div className="input-group">
                  <label htmlFor="imgDesc" className="input-group-addon">
                    <i className="fa fa-file-text-o"></i>
                  </label>

                  <input
                    id="imgDesc"
                    ref="imgDesc"
                    type="text"
                    className="form-control"
                    defaultValue={model.alt}
                  />
                </div>
              </div>

              <div className="form-group col-md-6">
                <label htmlFor="imgWidth">Largura</label>
                <div className="input-group">
                  <label htmlFor="imgWidth" className="input-group-addon">
                    <i className="fa fa-arrows-h"></i>
                  </label>

                  <input
                    id="imgWidth"
                    ref="imgWidth"
                    type="text"
                    className="form-control"
                    defaultValue={model.width}
                  />
                </div>
              </div>

              <div className="form-group col-md-6">
                <label htmlFor="imgHeight">Altura</label>
                <div className="input-group">
                  <label htmlFor="imgHeight" className="input-group-addon">
                    <i className="fa fa-arrows-v"></i>
                  </label>

                  <input
                    id="imgHeight"
                    ref="imgHeight"
                    type="text"
                    className="form-control"
                    defaultValue={model.height}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="modal-footer">
            <div className="col-md-12">
              <a href="javascript:void(0);" onClick={props.onCancel} className="marginTop10px text-muted pull-left">
                {'properties.btn.cancel'.translate()}
              </a>

              <button type="button" onClick={this.handleOnSubmit} className="btn btn-primary">
                {'properties.btn.save'.translate()}
              </button>
            </div>
          </div>
        </div>
      </ReactModal>
    );
  }
});

module.exports = PropertiesModal;
