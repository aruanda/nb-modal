'use strict';

var React = require('react');
var ReactModal = require('react-modal');

var FormModal = React.createClass({
  render: function() {
    return (
      <ReactModal
        className={'Modal__Bootstrap col-xs-12 col-sm-10 col-md-8 col-lg-6'}
        closeTimeoutMS={250}
        isOpen={this.props.show}
        onRequestClose={this.props.onLeave}
      >
        <div className="modal-content">
          <div className="modal-header">
            <button type="button" className="close" onClick={this.props.onLeave} aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>

            <h4 className="modal-title">
              { String(this.props.title).translate() }
            </h4>
          </div>

          <div className="modal-body">
            <div className="col-md-3 col-sm-6 col-xs-12">
              <div className="info-box bg-red">
                <span className="info-box-icon">
                  <i className="fa fa-comments-o"></i>
                </span>

                <div className="info-box-content">
                  <span className="info-box-text">Comments</span>
                  <span className="info-box-number">41,410</span>

                  <div className="progress">
                    <div className="progress-bar width70porcent"></div>
                  </div>

                  <span className="progress-description">
                    70% Increase in 30 Days
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div className="modal-footer">
            <div className="col-md-12">
              <a href="javascript:void(0);" className="marginTop10px text-muted pull-left" onClick={this.props.onLeave}>
                Fechar
              </a>

              <a href="javascript:void(0);" className="marginTop10px text-muted pull-left" onClick={this.props.onBack}>
                Listagem
              </a>

              <button type="button" className="btn btn-primary pull-right" onClick={this.props.onView}>
                Ver
              </button>
            </div>
          </div>
        </div>
      </ReactModal>
    );
  }
});

module.exports = FormModal;
