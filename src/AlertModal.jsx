'use strict';

var React = require('react');
var ReactModal = require('react-modal');

var AlertModal = React.createClass({
  render: function() {
    var btnOk;
    var btnClose;

    if (this.props.btnAction) {
      btnOk = (
        <div className="modal-footer">
          <button type="button" className="btn btn-default" onClick={this.props.btnAction}>
            { 'OK' }
          </button>
        </div>
      );

      btnClose = (
        <button type="button" className="close" onClick={this.props.btnAction}>
          <span aria-hidden="true">&times;</span>
          <span className="sr-only">Fechar</span>
        </button>
      );
    }

    return (
      <ReactModal
        className={'Modal__Bootstrap col-xs-12 col-sm-10 col-md-8 col-lg-6'}
        closeTimeoutMS={250}
        isOpen={this.props.show}
        onRequestClose={this.props.btnAction}
      >
        <div className="modal-content">
          <div className="modal-header">
            {btnClose}

            <h4 className="modal-title">
              { String(this.props.title).translate() }
            </h4>
          </div>

          <div className="modal-body">
            <div className={ 'alert alert-'.concat(this.props.type)}>
              { String(this.props.message).translate() }
            </div>
          </div>

          {btnOk}
        </div>
      </ReactModal>
    );
  }
});

module.exports = AlertModal;
